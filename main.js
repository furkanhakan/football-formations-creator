$(document).ready(function() {

    $("#formationSelect").change(function() {
        currentFormation = formations[$(this).val()];
        updateDraft();
    });

    // target elements with the "draggable" class
    interact('.draggable')
        .draggable({
            // enable inertial throwing
            inertia: true,
            // keep the element within the area of it's parent
            modifiers: [
                interact.modifiers.restrictRect({
                    restriction: 'parent',
                    endOnly: false
                })
            ],
            // enable autoScroll
            autoScroll: true,

            listeners: {
                // call this function on every dragmove event
                move: dragMoveListener,
            }
        })

    function dragMoveListener(event) {
        var target = event.target
            // keep the dragged position in the data-x/data-y attributes
        var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
        var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

        // translate the element
        target.style.transform =
            'translate(' + x + 'px, ' + y + 'px)'

        // update the posiion attributes
        target.setAttribute('data-x', x)
        target.setAttribute('data-y', y)
    }

    // this function is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener
});

function updateDraft() {
    
    const draftContainers = $(".draft-container");
    for (let id = 0; id < 11; id++) {
        const newPos = currentFormation[id];
        const destX = newPos.cx;
        const destY = newPos.cy;
        const posName = newPos.pos;
        $(draftContainers[id]).css({
            "transform": "translate(0,0)",
            "left": destX + "%",
            "top": destY + "%",
        });



        let $draftPos;
        if ($("#draft-pos-" + id).length) {
            $draftPos = $("#draft-pos-" + id);
        } else {
            $draftPos = $("<div class='draft-pos' id='draft-pos-" + id + "'> </div>");
            $(draftContainers[id]).append($draftPos);
        }

        $listPos = $("#list-pos-" + id);

        $draftPos.css("background-color", circleColors[posName]).html(posName);
        $listPos.css("background-color", circleColors[posName]).html(posName);
        
    }
}



const boxElements = document.querySelectorAll(".list-item");

boxElements.forEach(elem => {
  elem.addEventListener("dragstart", dragStart);
  elem.addEventListener("dragend", dragEnd);
  elem.addEventListener("dragenter", dragEnter);
  elem.addEventListener("dragover", dragOver);
  elem.addEventListener("dragleave", dragLeave);
  elem.addEventListener("drop", drop);
});

// Drag and Drop Functions

function dragStart(event) {
  event.target.classList.add("drag-start");
  event.dataTransfer.setData("text", event.target.id);
}

function dragEnd(event) {
  event.target.classList.remove("drag-start");
}

function dragEnter(event) {
  if(!event.target.classList.contains("drag-start")) {
    event.target.classList.add("drag-enter");
  }
}

function dragOver(event) {
  event.preventDefault();
}

function dragLeave(event) {
  event.target.classList.remove("drag-enter");
}

function drop(event) {
  event.preventDefault();
  event.target.classList.remove("drag-enter");
  const draggableElementId = event.dataTransfer.getData("text");
  const droppableElementId = event.target.id;
  if(draggableElementId !== droppableElementId) {
    const draggableElement = document.getElementById(draggableElementId);
    const droppableElementTextContent = event.target.querySelector("span").textContent;

    const draggableFootballerName = draggableElement.querySelector("span").textContent;
    const droppableFootballerName = event.target.querySelector("span").textContent;
    let dragId = draggableElement.querySelector("div").id.split("-")[2];
    let dropId = document.getElementById(droppableElementId).querySelector("div").id.split("-")[2];
    $("#footballer-name-" + dragId ).html(droppableFootballerName);
    $("#footballer-name-" + dropId).html(draggableFootballerName);

    event.target.querySelector("span").textContent = draggableElement.querySelector("span").textContent;
    draggableElement.querySelector("span").textContent = droppableElementTextContent;
  }
}